#!/bin/bash

for count in {1..10}; do
      echo "Pinging postgres database attempt "${count}
      if  $(nc -z database 5432) ; then
        echo "Can connect into database"
        hostname -I | awk '{print $1}'
        
        sleep 5
        python manage.py makemigrations
        python manage.py migrate
        break
      fi
      sleep 5
done
#python -u app/server/server.py &
python manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('reza', 'reza@reza.nl', 'adminpass')"
python -u manage.py runserver 0.0.0.0:8000