FROM python:3

WORKDIR /usr/src/app

RUN python3 -m venv env
RUN . env/bin/activate

COPY requirements.txt /usr/src/app
RUN pip install -r requirements.txt

RUN apt-get update && apt-get install -y netcat

COPY . .

ENTRYPOINT bash docker-entrypoint.sh