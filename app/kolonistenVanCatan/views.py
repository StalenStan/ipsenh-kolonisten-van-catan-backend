import random
from collections import namedtuple
import requests
import json
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render
from rest_framework import status, permissions, decorators
from rest_framework.decorators import api_view
from rest_framework.response import Response
from app.kolonistenVanCatan import constant
from app.kolonistenVanCatan.models import Friendship, Lobby
from django.utils.safestring import mark_safe

from app.kolonistenVanCatan.functionality import *


# User Views
@api_view(['POST'])
@decorators.permission_classes([permissions.AllowAny])
def register_user(request):
    username = request.data['username']
    password = request.data['password']
    if check_if_credentials_are_empty(username, password):
        response = create_response(constant.NO_CREDENTIALS)
        return response
    elif check_if_user_already_exists(username):
        response = create_response(constant.CONFLICT)
        return response
    else:
        return create_user(username, password)


@api_view(['GET'])
def get_current_user(request):
    current_user = {}
    user_id = request.user.id
    username = request.user.username
    if request.user.is_authenticated:
        current_user = {'user_id': user_id, 'user_username': username, 'password': ''}
    return JsonResponse(current_user)


@api_view(['GET'])
def get_users(request):
    col = ["id", "username", "first_name"]

    all_users = User.objects.all().values(*col)
    user_list = list(all_users)
    return JsonResponse(user_list, safe=False)


# Friend Views
@api_view(['POST'])
def add_friend(request):
    friend_username = request.data['friend']

    return create_friendship(request.user.id, friend_username)


@api_view(['GET'])
def get_friends_from_current_user(request):
    current_user_friendlist_with_usernames = retrieve_usernames_from_friend_list(request)

    return Response(current_user_friendlist_with_usernames)


# Lobby Views
@api_view(['POST'])
def challenge_friend(request):
    user_already_in_lobby = is_user_in_lobby(request.user.id)

    if user_already_in_lobby:
        Lobby.objects.filter(challenger_id=request.user.id).delete()

    if create_lobby(request):
        return Response({'message': 'Je zit in een lobby'})


@api_view(['GET'])
def get_users_from_current_lobby(request):
    user_in_current_lobby_list = get_users_from_requested_lobby(request)

    return Response(user_in_current_lobby_list)


@api_view(['GET'])
def get_lobby_for_current_user(request):
    all_lobbies = get_all_lobbies()
    user_id = request.user.id

    for lobby in all_lobbies:
        if user_id == lobby['challenger_id'] or lobby['challenged_id']:
            return Response(lobby)

    return Response({'message': 'No lobby found for this user'})


@api_view(['GET'])
def is_current_user_challenged(request):
    if is_user_in_lobby(request.user.id):
        return Response(True, status=status.HTTP_200_OK)


# Game Views
@api_view(['POST'])
def start_game(request):
    if set_is_game_active_in_model_to_true(request):
        return Response({'message': 'Het potje wordt gestart'}, status=status.HTTP_200_OK)
    else:
        Response({'message': 'Het potje is niet gestart'}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@decorators.permission_classes([permissions.AllowAny])
def get_dice_result(request):
    return Response(generate_dice_result())


@api_view(['POST'])
def update_gameboard(request):
    updated_gameboard = update_game_scores(request.data)
    return Response(updated_gameboard)


@api_view(['POST'])
def end_turn(request):
    return Response(set_player_id_turn_to_other_player_id(request))
