from django.apps import AppConfig


class KolonistenVanCatanConfig(AppConfig):
    name = 'app.kolonistenVanCatan'
