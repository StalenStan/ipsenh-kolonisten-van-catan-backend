from django.urls import path
from . import views, functionality

urlpatterns = [
    # User URLs
    path('api/registerUser', views.register_user, name='registerUser'),
    path('api/currentUser', views.get_current_user, name='currentUser'),
    path('api/users', views.get_users, name='users'),

    # Friend URLs
    path('api/addFriend', views.add_friend, name="addFriend"),
    path('api/friendsFromCurrentUser', views.get_friends_from_current_user, name="friendsFromCurrentUser"),

    # Lobby URLs
    path('api/challengeFriend', views.challenge_friend, name="challengeFriend"),
    path('api/usersFromCurrentLobby', views.get_users_from_current_lobby, name="usersFromCurrentLobby"),
    path('api/lobbyForCurrentUser', views.get_lobby_for_current_user, name="lobbyForCurrentUser"),
    path('api/isCurrentUserChallenged', views.is_current_user_challenged, name="isCurrentUserChallenged"),

    path('', functionality.index, name='index'),
    path('<str:room_name>/', functionality.chatroom, name='chatroom'),

    # Game URLs
    path('api/startGame', views.start_game, name="startGame"),
    path('api/diceResult', views.get_dice_result, name="diceResult"),
    path('api/updateGameboard', views.update_gameboard, name="updateGameboard"),
    path('api/endTurn', views.end_turn, name="endTurn"),
]
