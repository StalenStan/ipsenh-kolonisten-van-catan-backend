from django.test import TestCase, Client
from django.urls import reverse
from app.kolonistenVanCatan.models import Friendship
import json


class TestViews(TestCase):

    def test_register_user_POST(self):
        client = Client()
        response = client.post(reverse('registerUser'), {'username': 'rezaTest', 'password': 'test'})

        self.assertEquals(response.status_code, 200)

    def test_registerUserWithEmptyUsername_ReturnsStatusCode400_POST(self):
        client = Client()
        response = client.post(reverse('registerUser'), {'username': '', 'password': 'test'})

        self.assertEquals(response.status_code, 400)

    def test_registerUserMultiple_ReturnsStatusCode409_POST(self):
        client = Client()
        client.post(reverse('registerUser'), {'username': 'rezaTest', 'password': 'test'})
        response = client.post(reverse('registerUser'), {'username': 'rezaTest', 'password': 'test'})

        self.assertEquals(response.status_code, 409)

    def test_currentUser_GET(self):
        client = Client()
        client.post(reverse('registerUser'), {'username': 'rezaTest', 'password': 'test'})
        response = client.get(reverse('currentUser'))

        self.assertEquals(response.status_code, 200)

    def test_users_GET(self):
        client = Client()
        response = client.get(reverse('users'))

        self.assertEquals(response.status_code, 200)

    # def test_add_friend_POST(self):
    #     client = Client()
    #     client.post(reverse('registerUser'), {'username': 'rezaTest', 'password': 'test'})
    #     client.post(reverse('registerUser'), {'username': 'stanTest', 'password': 'test'})
    #     response = client.post(reverse('addFriend'), {'friend': 'stanTest'})
    #
    #     self.assertEquals(response.status_code, 200)

    def test_diceResult_GET(self):
        client = Client()
        response = client.get(reverse('diceResult'))

        self.assertEquals(response.status_code, 200)
