from django.test import SimpleTestCase
from django.urls import reverse, resolve
from app.kolonistenVanCatan.views import *
from app.kolonistenVanCatan.functionality import chatroom


class TestUrls(SimpleTestCase):
    # User URL Tests
    def test_registerUser_url_is_resolved(self):
        url = reverse('registerUser')
        self.assertEquals(resolve(url).func, register_user)

    def test_currentUser_url_is_resolved(self):
        url = reverse('currentUser')
        self.assertEquals(resolve(url).func, get_current_user)

    def test_users_url_is_resolved(self):
        url = reverse('users')
        self.assertEquals(resolve(url).func, get_users)

    # Friend URL Tests
    def test_addFriend_url_is_resolved(self):
        url = reverse('addFriend')
        self.assertEquals(resolve(url).func, add_friend)

    def test_friendsFromCurrentUser_url_is_resolved(self):
        url = reverse('friendsFromCurrentUser')
        self.assertEquals(resolve(url).func, get_friends_from_current_user)

    # Lobby URL Tests
    def test_challengeFriend_url_is_resolved(self):
        url = reverse('challengeFriend')
        self.assertEquals(resolve(url).func, challenge_friend)

    def test_usersFromCurrentLobby_url_is_resolved(self):
        url = reverse('usersFromCurrentLobby')
        self.assertEquals(resolve(url).func, get_users_from_current_lobby)

    def test_lobbyForCurrentUser_url_is_resolved(self):
        url = reverse('lobbyForCurrentUser')
        self.assertEquals(resolve(url).func, get_lobby_for_current_user)

    def test_isCurrentUserChallenged_url_is_resolved(self):
        url = reverse('isCurrentUserChallenged')
        self.assertEquals(resolve(url).func, is_current_user_challenged)

    def test_index_url_is_resolved(self):
        url = reverse('index')
        self.assertEquals(resolve(url).func, index)

    # Game URL Tests
    def test_startGame_url_is_resolved(self):
        url = reverse('startGame')
        self.assertEquals(resolve(url).func, start_game)

    def test_diceResult_url_is_resolved(self):
        url = reverse('diceResult')
        self.assertEquals(resolve(url).func, get_dice_result)

    def test_updateGameboard_url_is_resolved(self):
        url = reverse('updateGameboard')
        self.assertEquals(resolve(url).func, update_gameboard)

    def test_endTurn_url_is_resolved(self):
        url = reverse('endTurn')
        self.assertEquals(resolve(url).func, end_turn)
