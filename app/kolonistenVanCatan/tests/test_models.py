from django.test import TestCase
from app.kolonistenVanCatan.models import Friendship, Lobby


class TestModels(TestCase):

    def setUp(self):
        self.friendship = Friendship.objects.create(
            user_id='1',
            friend_id='2')
        self.lobby = Lobby.objects.create(
            challenger_id='1',
            challenged_id='2',
            player_id_turn='2',
            is_game_active=True

        )


    def test_creationFriendshipModel_returnsUserIdIsOne(self):
        self.assertEquals(self.friendship.user_id, '1')

    def test_creationFriendshipModel_returnsFriendIdIsTwo(self):
        self.assertEquals(self.friendship.friend_id, '2')

    def test_creationLobbyModel_returnsChallengerIdIsOne(self):
        self.assertEquals(self.lobby.challenger_id, '1')

    def test_creationLobbyModel_returnsChallengedIdIsTwo(self):
        self.assertEquals(self.lobby.challenged_id, '2')
    
    def test_creationLobbyModel_returnsPlayerIdTurnOne(self):
        self.assertEquals(self.lobby.player_id_turn, '2')
    
    def test_creationLobbyModel_IsGameActiveReturnsTrue(self):
        self.assertEquals(self.lobby.is_game_active, True)

