import random
from collections import namedtuple
import requests
import json
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render
from rest_framework import status, permissions, decorators
from rest_framework.decorators import api_view
from rest_framework.response import Response
from app.kolonistenVanCatan import constant
from app.kolonistenVanCatan.models import Friendship, Lobby
from django.utils.safestring import mark_safe
import json


# User Functions
def check_if_credentials_are_empty(username, password):
    if username == '' or password == '':
        return True


def create_response(constant_status):
    if constant_status == constant.NO_CREDENTIALS:
        message = constant.NO_USERNAME_OR_PASSWORD
        http_status = status.HTTP_400_BAD_REQUEST
        header = {constant.STATUS: http_status, constant.MESSAGE: message}
        return Response(header, status=http_status)
    elif constant_status == constant.CONFLICT:
        message = constant.USER_EXISTS
        http_status = status.HTTP_409_CONFLICT
        header = {constant.STATUS: http_status, constant.MESSAGE: message}
        return Response(header, status=http_status)


def check_if_user_already_exists(username_from_request):
    return User.objects.filter(username=username_from_request).exists()


def create_user(username, password):
    user = User.objects.create_user(username=username, password=password)
    user.save()
    message = constant.REGISTER_SUCCEED
    http_status = status.HTTP_200_OK
    header = {constant.STATUS: http_status, constant.MESSAGE: message}
    return Response(header, status=http_status)


def get_all_users():
    col = ["id", "username", "first_name"]

    all_users = User.objects.all().values(*col)
    user_list = create_list_from_array(all_users)
    return user_list


def create_list_from_array(array):
    list_from_array = list(array)

    return list_from_array


def retrieve_user_from_id(user_id):
    all_users_list = get_all_users()

    for user in all_users_list:
        if user_id == user['id']:
            return user


# Friend Functions
def create_friendship(current_user_id, friend_username):
    all_users = get_all_users()

    for user in all_users:
        if user['username'] == friend_username:
            if check_if_friendship_already_exists(current_user_id, user['id']):
                return Response({'message': 'Friendship relation already exists'}, status=status.HTTP_409_CONFLICT)
            else:
                friend_user_id = user['id']
                save_friendship(current_user_id, friend_user_id)

                # Creates a friendship for the other user as well
                save_friendship(friend_user_id, current_user_id)
                return Response({'message': 'Je hebt een vriend toegevoegd'})
    return Response({'message': 'Vriend bestaat niet'}, status=status.HTTP_400_BAD_REQUEST)


def save_friendship(user_id, friend_id):
    friendship = Friendship(user_id=user_id, friend_id=friend_id)
    friendship.save()


def retrieve_usernames_from_friend_list(request):
    current_user_friendlist = get_all_friends(request.user.id)
    user_list = get_all_users()

    current_user_friendlist_with_usernames = []

    for friend in current_user_friendlist:
        friend_id = int(friend['friend_id'])
        for user in user_list:
            user_id = int(user['id'])
            if friend_id == user_id:
                current_user_friendlist_with_usernames.append(user)

    return current_user_friendlist_with_usernames


def check_if_friendship_already_exists(current_user_id, friendId):
    return Friendship.objects.filter(user_id=current_user_id, friend_id=friendId).exists()


def get_all_friends(user_id):
    col = ["id", "user_id", "friend_id"]

    user_friend_list = []

    all_friends = Friendship.objects.all().values(*col)
    friends_list = create_list_from_array(all_friends)
    for friend in friends_list:
        if user_id == int(friend['user_id']):
            user_friend_list.append(friend)

    return user_friend_list


# Lobby Functions
def is_user_in_lobby(user_id):
    lobby_list = get_all_lobbies()

    is_user_in_lobby = False

    for lobby in lobby_list:
        if user_id == lobby["challenger_id"]:
            is_user_in_lobby = True
        elif user_id == lobby["challenged_id"]:
            is_user_in_lobby = True

    return is_user_in_lobby


def get_users_from_requested_lobby(request):
    lobby_list = get_all_lobbies()
    user_id = request.user.id

    user_list = []

    for lobby in lobby_list:
        if user_id == lobby['challenger_id'] or user_id == lobby['challenged_id']:
            user_list.append(retrieve_user_from_id(lobby['challenger_id']))
            user_list.append(retrieve_user_from_id(lobby['challenged_id']))

    return user_list


def get_all_lobbies():
    col = ["id", "challenger_id", "challenged_id"]

    all_lobbies = Lobby.objects.all().values(*col)
    lobby_list = create_list_from_array(all_lobbies)

    return lobby_list


def create_lobby(request):
    friend_username = request.data['friend']
    all_users = get_all_users()

    is_lobby_created = False

    for user in all_users:
        if user['username'] == friend_username:
            coin_flip = random.randint(1, 2)
            player_id_turn = request.user.id

            if coin_flip == 2:
                player_id_turn = user['id']

            request_user_id = request.user.id

            lobby = save_lobby(request_user_id, user['id'], player_id_turn, False)
            create_chat(request, lobby)
            is_lobby_created = True

            return is_lobby_created


def save_lobby(challenger_id, challenged_id, player_id_turn, is_game_active):
    lobby = Lobby(challenger_id=challenger_id, challenged_id=challenged_id, player_id_turn=player_id_turn, is_game_active=is_game_active)
    lobby.save()

    return lobby


def index(request):
    return render(request, 'kolonistenVanCatan/index.html')


def chatroom(request, room_name):
    return render(request, 'kolonistenVanCatan/room.html', {
        'room_name_json': mark_safe(json.dumps(room_name))
    })


def create_chat(request, lobby):
    return render(request, 'kolonistenVanCatan/room.html', {
        'room_name_json': mark_safe(json.dumps(lobby.pk))
    })


# Game Functions
def set_is_game_active_in_model_to_true(request):
    lobby_list = get_all_lobbies()
    user_id = request.user.id

    is_game_active = False

    for lobby in lobby_list:
        if user_id == lobby["challenger_id"]:
            Lobby.objects.filter(challenger_id=user_id).update(is_game_active=True)
            is_game_active = True

            return is_game_active


def generate_dice_result():
    dice_result = random.randint(1, 6)
    dice_result_in_json = {'diceResult': dice_result}

    return dice_result_in_json


def update_game_scores(gameCard):
    placeResourceDirection = ['left', 'right']
    exactPlaceLocation = ['bottomleft', 'bottomright', 'topleft', 'topright']
    street = gameCard["gameBoard"]["centerStreet"]
    diceResult = gameCard["diceResult"]["diceResult"]
    for search in street:
        for leftOrRight in placeResourceDirection:
            if search == leftOrRight:
                for location in exactPlaceLocation:
                    for jj in street[leftOrRight]:
                        if jj == location and street[leftOrRight][location] is not None and street[leftOrRight][location]['diceNumber'] == diceResult:
                            street[leftOrRight][location]['value'] = int(street[leftOrRight][location]['value']) + 1
    gameCard["gameBoard"]["centerStreet"] = street
    return gameCard


def set_player_id_turn_to_other_player_id(request):
    lobby_list = get_all_lobbies()
    user_id = request.user.id

    for lobby in lobby_list:
        if user_id == lobby['challenger_id']:
            Lobby.objects.filter(challenger_id=user_id).update(player_id_turn=lobby['challenged_id'])
        else:
            Lobby.objects.filter(challenged_id=user_id).update(player_id_turn=lobby['challenger_id'])

    return ({'message': 'Je hebt je beurt beeindigd'})
