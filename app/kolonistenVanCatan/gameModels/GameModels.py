import enum


class Hoi():
    left = int
    right = int


class diceResult:
    diceResult = int


class ResourceNames(enum.Enum):
    GRAIN = 'graan'
    WOOD = 'hout'
    ORE = 'ore'
    SHEEP = 'schaap'
    STONE = 'steen'
    GOLD = 'goud'


class TileModel:
    resourceImagePath = ''


class ResourceTileModel(TileModel):
    resourceName = ResourceNames.GRAIN
    value = int
    diceNumber = int


class VillageTileModel(TileModel):
    # from .Test.StreetTileModel import StreetTileModel
    topleft = ResourceTileModel
    topright = ResourceTileModel
    bottomleft = ResourceTileModel
    bottomright = ResourceTileModel
    left = None
    right = None


class StreetTileModel(TileModel):
    left = VillageTileModel
    right = VillageTileModel


class GameBoardModel:
    centerStreet = StreetTileModel


class DiceAndBoardModel:
    diceResult = int
    gameBoard = GameBoardModel
