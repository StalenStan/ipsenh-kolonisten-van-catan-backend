from django.db import models


class Friendship(models.Model):
    user_id = models.IntegerField()
    friend_id = models.IntegerField()


class Lobby(models.Model):
    challenger_id = models.IntegerField()
    challenged_id = models.IntegerField()
    player_id_turn = models.IntegerField()
    is_game_active = models.BooleanField()
